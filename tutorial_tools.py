#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
tutorial functions
"""
#%%%
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib import rc
#from datetime import date
import matplotlib.patches as ptc

def triang(x,phase,length,amplitude):
    alpha=(amplitude)/(length/2)
    return -amplitude/2+amplitude*((x-phase)%length==length/2) \
            +alpha*((x-phase)%(length/2))*((x-phase)%length<=length/2) \
            +(amplitude-alpha*((x-phase)%(length/2)))*((x-phase)%length>length/2)

def ts_compare(td,r,rsim):
    plt.figure(figsize=(10,20))
    ax1=plt.subplot(2,1,1)
    plt.plot(td,r)
    plt.title('Darwin daily rainfall')
    plt.xlabel('time')
    plt.ylabel('rainfall amount [mm]')
    plt.subplot(2,1,2,sharex=ax1,sharey=ax1)
    plt.plot(td,rsim)
    plt.title('Simulation')
    plt.xlabel('time')
    plt.ylabel('rainfall amount [mm]')

def hist_comp(r,rsim):
    nr=np.shape(rsim)[1]
    plt.figure()
    nbins=20 # number of bins
    bin_edges=np.linspace(0,np.max(r),num=nbins+1) # bin edges
    bin_centers=0.5*(bin_edges[1:] + bin_edges[:-1]) # bin centers
    y,x=np.histogram(r,bin_edges) # data histogram (line)
    ax1=plt.plot(bin_centers,y,'-o',label='data')
    ysim=np.empty([nr,nbins]) # simulation histogram (boxplot)
    for i in range(nr):
        ysim[i,:],x=np.histogram(rsim[:,i],bin_edges)
    plt.violinplot(ysim,positions=bin_centers,widths=20)
    ax2 = ptc.Patch(color='orange') # fake handle for legend
    # cosmetics
    plt.yscale('log')
    plt.title('Daily rainfall frequency distribution')
    plt.xlabel('rainfall amount [mm]')
    plt.ylabel('absolute frequency')
    plt.legend([ax1[0],ax2],['data','simulation'])
    plt.ylim([0.90,plt.gca().get_ylim()[1]])

def monthly_mean(r,rsim,td):
    nr=np.shape(rsim)[1]
    tm=np.array([td[i].strftime('%m') for i in range(np.shape(td)[0])]) # extract month number
    mnum=np.unique(tm) # list of month numbers
    rm=[np.nanmean(r[tm==i]) for i in mnum] # mean mointhly rainfall for data
    rsimm=np.empty([nr,len(mnum)])
    for j in range(nr):
        rsimm[j,:]=[np.nanmean(rsim[tm==i,j]) for i in mnum] # the same for simulations
    mnames=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Oct', 'Nov','Dec']
    plt.figure()
    ax1=plt.plot(mnames,rm,'-o')
    plt.title('Darwin monthly mean daily rianfall [mm]')
    plt.violinplot(rsimm,positions=np.arange(len(mnum)))
    ax2 = ptc.Patch(color='orange') # fake handle for legend
    plt.legend([ax1[0],ax2],['data','simulation'])

def ds_length(r):
    rdw=r>0
    dl=[]
    n=0
    for i in range(len(rdw)):
        if rdw[i]==0:
            n=n+1
        else:
            dl.append(n)
            n=0
    dl=np.array(dl)
    return dl[dl!=0]

def ds_hist(r,rsim):
    nr=np.shape(rsim)[1]
    rl=ds_length(r)
    rsiml=np.array([ds_length(rsim[:,i]) for i in range(nr)])
    plt.figure()
    nbins=20 # number of bins
    bin_edges=np.linspace(0,np.max(rl),num=nbins+1) # bin edges
    bin_centers=0.5*(bin_edges[1:] + bin_edges[:-1]) # bin centers
    y,x=np.histogram(rl,bin_edges) # data histogram (line)
    ax1=plt.plot(bin_centers,y,'-o',label='data')
    ysim=np.empty([nr,nbins]) # simulation histogram (boxplot)
    for i in range(nr):
        ysim[i,:],x=np.histogram(rsiml[i],bin_edges)
    plt.violinplot(ysim,positions=bin_centers,widths=10)
    ax2 = ptc.Patch(color='orange') # fake handle for legend
    # cosmetics
    plt.yscale('log')
    plt.title('Dry-spell frequency distribution')
    plt.xlabel('spell length [days]')
    plt.ylabel('absolute frequency')
    plt.legend([ax1[0],ax2],['data','simulation'])
    