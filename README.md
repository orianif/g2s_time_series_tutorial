# TUTORIAL: TIME-SERIES SIMULATION AND ANALYSIS USING THE MULTIPLE-POINT ALGORITHM G2S, AN EXAMPLE ON DAILY RAINFALL TIME-SERIES #


### What is this repository for? ###

This tutorial has the goal to provide an example of usage of the MPS software G2S to make multiple-point simulations of time-series given a training dataset.
MPS simulation allows generating multiple stochastic realizations of a given dataset, preserving realistic features and statistical dependencies. These can be used to make uncertainty estimation, ensemble predictions, and missing data imputation.

### How to begin ###

Before going through the tutorial, download and install the G2S software and python interface: https://github.com/GAIA-UNIL/G2S

Open tutorial.py in your python interface/editor and follow the instructions.
You can execute the tutorial in Spyder, for an easy step-by-step operation using code blocks.

### Dependencies ###

_ Python 3

_ Python packages: g2s, numpy, matplotlib, datetime, tutorial_tools (local package).

### Need help? ###

Write Fabio : fabio (dot) oriani (at) protonmail (dot com)

### How to cite this software ###

Quantile Sampling algorithm:
Gravey et al.
Quantile Sampling: a robust and simplified pixel-based multiple-point simulation approach, in preparation 

MPS multivariate setups for time-series simulation:
Oriani F., Straubhaar J., Renard P., Mariethoz G.
Hydrol. Earth Syst. Sci., 18, 3015–3031, 2014
https://doi.org/10.5194/hess-18-3015-2014

Oriani, F., Mehrotra, R., Mariethoz, G. et al. 
Stoch Environ Res Risk Assess (2018) 32: 321. 
https://doi.org/10.1007/s00477-017-1414-z 

Oriani F, Borghi A, Straubhaar J, Mariethoz G, Renard P (2016). 
Environmental Modelling and Software 86, 264-276, 
https://doi.org/10.1016/j.envsoft.2016.10.002

### License of this tutorial ###

Copyright (C) 2019  Fabio Oriani

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
