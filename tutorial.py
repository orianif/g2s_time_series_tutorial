#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#%% TUTORIAL: TIME-SERIES SIMULATION AND ANALYSIS USING THE MULTIPLE-POINT ALGORITHM G2S:
#   AN EXAMPLE ON DAILY RAINFALL TIME-SERIES
#
# This tutorial has the goal to provide an example of usage of the MPS software G2S
# to make multiple-point simulations of time-series given a training dataset.
# MPS simulation allows generating multiple stochastic realizations of a given dataset 
# preserving realistic features and statistical dependencies. These can be used to 
# make uncertainty estimation, ensemble predictions, and missing data imputation.
#
# IMPORTANT: before going through the tutorial, download and install the G2S software
# here: https://github.com/GAIA-UNIL/G2S
#
# RELEVANT CITATIONS
# Quantile Sampling algorithm:
# Gravey et al.
# Quantile Sampling: a robust and simplified pixel-based multiple-point simulation approach
# in preparation 
#
# MPS multivariate setups for time-series simulation:
# Oriani F., Straubhaar J., Renard P., Mariethoz G.
# Hydrol. Earth Syst. Sci., 18, 3015–3031, 2014
# https://doi.org/10.5194/hess-18-3015-2014
#
# Oriani, F., Mehrotra, R., Mariethoz, G. et al. 
# Stoch Environ Res Risk Assess (2018) 32: 321. 
# https://doi.org/10.1007/s00477-017-1414-z 
# 
# Oriani F, Borghi A, Straubhaar J, Mariethoz G, Renard P (2016). 
# Environmental Modelling and Software 86, 264-276, 
# https://doi.org/10.1016/j.envsoft.2016.10.002
#
# You can execute the tutorial in Spyder, for an easy step-by-step operation using code blocks.

#%% dependency packages
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'family' : 'DejaVu Sans',
        'size'   : 14}
rc('font', **font)
from g2s import run as g2s
from datetime import date
import tutorial_tools as tt # present in the local directory

#%% import data
r=np.genfromtxt("data/darwin.csv",delimiter=",",skip_header=1,usecols=(5)) # rainfall amount
t=np.genfromtxt("data/darwin.csv",delimiter=",",skip_header=1,usecols=(2,3,4),dtype="int") # timestamp
td=[date(t[i,0],t[i,1],t[i,2]) for i in range(np.shape(t)[0])] # stimestamp to date object
plt.figure(figsize=(20,10))
plt.title('Darwin daily rainfall')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')
plt.plot(td,r)

#%% EXERCISE 1: simulation using G2S, rainfall amount only

# SERVER SETUP
# IMPORTANT: first launch the G2S server on your host machine (see instruction in the repository, address above) 
serverAddress='localhost' # put here your server address. example:'tesla.gaia.unil.ch'

# algorithm setup
ti = r # training image
di = np.empty_like(ti)*np.nan # empty simulation grid (destimantion image)
dt = np.array([0]) # data type (numpy array, 1 element per variable): 0 = continuous, 1 = categorical
k = 3 # quantile threshold
n = 20 # maximum number of neighbors (the n informed and closest)
ki = np.ones([101,1]) # uniform kernel
s = 100 # initial seed
jb = 8 # number of parallel jobs
nr = 10 # number of realizations

# launch the simulation
rsim=np.empty([len(r),nr])
for i in range(nr):
    s=s+1
    print("realization " + str(i))
    simout=g2s('-sa',serverAddress,'-a','qs','-ti',ti,'-di',di,'-dt',dt,'-ki',ki,'-k', k,'-n', n,'-s', s, '-j',jb);
    rsim[:,i]=simout[0]
    
#%% visual comparison
plt.figure(figsize=(10,20))
ax1=plt.subplot(2,1,1)
plt.plot(td,r)
plt.title('Darwin daily rainfall')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')
plt.subplot(2,1,2,sharex=ax1,sharey=ax1)
plt.plot(td,rsim)
plt.title('Simulation')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')

#%% histogram
tt.hist_comp(r,rsim)

#%% monthly mean rainfall
tt.monthly_mean(r,rsim,td)

#%% auxiliary variables: calendar position
tr1=tt.triang(np.arange(len(td)),0,365,1)
tr2=tt.triang(np.arange(len(td)),365/4,365,1)
plt.figure()
plt.plot(r)
plt.plot(tr1*50)
plt.plot(tr2*50)

#%% EXERCISE 2: simulation using G2S, multivariate rainfall + calendar <----
serverAddress='localhost' # used server address
ti = np.stack([tr1,tr2,r],axis=1) # training image <----
di = np.empty_like(ti)*np.nan # empty simulation grid (destimantion image)
di[:,0:2]=ti[:,0:2] # add annual seasonality as conditioning variables <----
dt = np.array([0,0,0]) # data type (numpy array, 1 element per variable): 0 = continuous, 1 = categorical <----
s = 100 # initial seed
jb = 8 # number of parallel jobs
nr = 10 # number of realizations
k = 30 # quantile threshold (take a random value among the best k-neighbors)
ki = np.ones([100,3]) # uniform multivariate kernel <----
n = 20 # maximum number of neighbors (the n informed and closest)
# launch simulation
rsim=np.empty([len(r),nr]) # preallocate output array <----
for i in range(nr):
    s=s+1
    print("realization " + str(i))
    simout=g2s('-sa',serverAddress,'-a','qs','-ti',ti,'-di',di,'-dt',dt,'-ki',ki,'-k', k,'-n', n,'-s', s, '-j',jb);
    rsim[:,i]=simout[0][:,2] # <---- retrieve rainfall amount only

#%% visual comparison
plt.figure(figsize=(10,20))
ax1=plt.subplot(2,1,1)
plt.plot(td,r)
plt.title('Darwin daily rainfall')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')
plt.subplot(2,1,2,sharex=ax1,sharey=ax1)
plt.plot(td,rsim[:,0])
plt.title('Simulation')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')

#%% histogram
tt.hist_comp(r,rsim)

#%% monthly mean rainfall
tt.monthly_mean(r,rsim,td)

#%% dry-spell histogram
tt.ds_hist(r,rsim)

#%% auxiliary variables: dry/wet indicator
rdw=r>0
plt.figure()
plt.plot(r)
plt.plot(rdw*10)

#%% EXERCISE 2: simulation using G2S, multivariate rainfall + calendar + dry spell <----
serverAddress='localhost' # used server address
ti = np.stack([tr1,tr2,rdw,r],axis=1) # training image <----
di = np.empty_like(ti)*np.nan # empty simulation grid (destimantion image)
di[:,0:2]=ti[:,0:2] # add annual seasonality as conditioning vairables <----
dt = np.array([0,0,1,0]) # data type (numpy array, 1 element per variable): 0 = continuous, 1 = categorical <----
s = 100 # initial seed
jb = 8 # number of parallel jobs
nr = 10 # number of realizations
k = 30 # quantile threshold (take a random value among the best k-neighbors)
ki = np.ones([200,4]) # uniform multivariate kernel  <----
n = 20 # maximum number of neighbors (the n informed and closest)
# launch simulation
rsim=np.empty([len(r),nr]) # preallocate output array <----
for i in range(nr):
    s=s+1
    print("realization " + str(i))
    simout=g2s('-sa',serverAddress,'-a','qs','-ti',ti,'-di',di,'-dt',dt,'-ki',ki,'-k', k,'-n', n,'-s', s, '-j',jb);
    rsim[:,i]=simout[0][:,3] # <---- retrieve rainfall amount only

#%% visual comparison
plt.figure(figsize=(10,20))
ax1=plt.subplot(2,1,1)
plt.plot(td,r)
plt.title('Darwin daily rainfall')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')
plt.subplot(2,1,2,sharex=ax1,sharey=ax1)
plt.plot(td,rsim[:,0])
plt.title('Simulation')
plt.xlabel('time')
plt.ylabel('rainfall amount [mm]')

#%% histogram
tt.hist_comp(r,rsim)

#%% monthly mean rainfall
tt.monthly_mean(r,rsim,td)

#%% dry-spell histogram
tt.ds_hist(r,rsim)

####### END OF THE TUTORIAL #############
